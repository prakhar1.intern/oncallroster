#!/bin/bash

rm -rf ./pillar
rm -rf ./salt
multipass transfer --recursive primary:/srv/pillar .
multipass transfer --recursive primary:/srv/salt .
