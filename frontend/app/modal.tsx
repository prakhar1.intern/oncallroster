import { ChangeEvent, useEffect, useState } from 'react';
import './modal.css'

interface ModalProps {
  handleClose: () => void;
  handleSubmit: (data: JsonResponse) => void;
  show: boolean;
  employee: JsonResponse;
}

interface JsonResponse {
  id: number,
  firstName: string,
  lastName: string
}

const Modal = ({ handleClose, handleSubmit, show, employee }: ModalProps) => {
  const [data, setData] = useState<JsonResponse>(employee);
  console.log(data);
  console.log(employee);

  useEffect(() => {
    if (employee) {
      setData(employee);
    }
  }, [employee])

  const updateData = (e: ChangeEvent<HTMLInputElement>) => {
    console.log(data);
    setData({
      ...data,
      [e.target.name]: e.target.value
    })
  }

  const submit = (e: any) => {
    e.preventDefault();
    if (data.firstName === '' || !data.firstName || data.lastName === '' || !data.lastName) {
      console.log('firstName and lastName both required');
      return;
    }
    console.log();
    handleSubmit(data);
  }

  return (
    show ?
      <div>
        <section className="modal-main">
          <form onSubmit={e => submit(e)}>
            <input autoFocus={true} name='firstName' value={data.firstName || ''} placeholder="First Name" onChange={updateData} />
            <input name='lastName' value={data.lastName || ''} placeholder="Last Name" onChange={updateData} />
            <button type="submit">
              Submit
            </button>
            <button type="button" onClick={_ => handleClose()}>
              Close
            </button>
          </form>
        </section>
      </div>
      : null
  );
}

export default Modal;
