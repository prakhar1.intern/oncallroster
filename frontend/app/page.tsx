"use client"
import { useEffect, useReducer, useState } from 'react';
import Modal from "./modal"
import styles from './page.module.css'

interface JsonResponse {
  id: number,
  firstName: string,
  lastName: string
}

function RenderRoster() {
  const [rosterModified, setRosterModified] = useState(true)
  const [apiResponse, setApiResponse] = useState<JsonResponse[]>([]);
  const [showForm, setShowForm] = useState(false);
  const [employee, setEmployee] = useState<JsonResponse>({} as JsonResponse);

  const endPoint = "/api/roster"

  const renderList = async () => {
    const response = await fetch(endPoint);
    const jsonResponse = await response.json();
    setApiResponse(jsonResponse);
  }

  const deleteEmployee = async (id: number) => {
    await fetch(endPoint + '/' + id, {
      method: 'DELETE'
    });
    console.log("DELETED");
    setRosterModified(true);
  }

  const clearRoster = async () => {
    await fetch(endPoint, {
      method: 'DELETE'
    });
    console.log("CLEARED");
    setRosterModified(true);
  }

  useEffect(() => {
    if (rosterModified) {
      renderList();
      setRosterModified(false);
      console.log('UPDATED');
    }
    console.log('NOT-UPDATED');
  }, [rosterModified, setRosterModified]);

  const submitCallback = async (data: JsonResponse) => {
    const method = data.id ? 'PUT' : 'POST';
    await fetch(endPoint + (data.id ? '/' + data.id : ''), {
      method: method,
      headers: { 'Content-Type': 'application/json' },
      body: JSON.stringify(data)
    });
    console.log("CREATED/UPDATED");
    setRosterModified(true);
    setShowForm(false);
  }

  const addEmployee = () => {
    setEmployee({} as JsonResponse);
    setShowForm(true);
  }

  const editEmployee = (emp: JsonResponse) => {
    setEmployee(emp);
    console.log(emp);
    console.log(employee);
    setShowForm(true);
  }

  return (
    <>
      <Modal
        handleClose={() => setShowForm(false)}
        handleSubmit={submitCallback}
        show={showForm}
        employee={employee}
      />
      <div>
        <h1 className={styles.title}>On-call Roster</h1>
        <div className={styles.title}>
          <a className={styles.button} onClick={addEmployee}>[add]</a>
          <a className={styles.button} onClick={clearRoster}>[clear]</a>
        </div>
        <p>{apiResponse.map(
          (employee: JsonResponse) =>
            <li className={styles.item} key={employee.id}>
              <a className={styles.name}>{employee.firstName + ' ' + employee.lastName}</a>
              <a className={styles.button} onClick={_ => editEmployee(employee)} >[edit]</a>
              <a className={styles.button} onClick={_ => deleteEmployee(employee.id)}>[delete]</a>
            </li>
        )
        }</p>
      </div>
    </>
  );
};

export default function Home() {
  return (
    <RenderRoster />
  )
}
