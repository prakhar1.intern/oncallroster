import { Module } from '@nestjs/common';
import { ConfigModule } from '@nestjs/config';
import { ServeStaticModule } from '@nestjs/serve-static';
import { TypeOrmModule } from '@nestjs/typeorm';
import { join } from 'path';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { Employee } from './roster/models/employee.entity';
import { RosterModule } from './roster/roster.module';

@Module({
  imports: [
    ConfigModule.forRoot({
      isGlobal: true,
    }),
    TypeOrmModule.forRoot({
      type: 'postgres',
      host: process.env.POSTGRES_HOST,
      port: parseInt(process.env.POSTGRES_PORT),
      username: process.env.POSTGRES_USER,
      password: process.env.POSTGRES_PASSWORD,
      database: process.env.POSTGRES_DATABASE,
      entities: [Employee],
      // autoLoadEntities: true,
      synchronize: true,
    }),
    RosterModule,
    ServeStaticModule.forRoot({
      rootPath: join(__dirname, '../../frontend/out'),
    }),
  ],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {}
