import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Observable, from } from 'rxjs';
import { InsertResult, Repository } from 'typeorm';
import { Employee } from '../models/employee.entity';

@Injectable()
export class RosterService {
  constructor(
    @InjectRepository(Employee)
    private employeeRepository: Repository<Employee>,
  ) {}

  findAll(): Observable<Employee[]> {
    return from(this.employeeRepository.find());
  }

  async removeAll(): Promise<void> {
    await this.employeeRepository.clear();
  }

  insert(employee: Employee): Promise<InsertResult> {
    return this.employeeRepository.insert(employee);
  }

  async remove(id: number): Promise<void> {
    await this.employeeRepository.delete(id);
  }

  update(employee: Employee): Observable<Employee> {
    return from(this.employeeRepository.save(employee));
  }
}
