import { Module } from '@nestjs/common';
import { RosterService } from './services/roster.service';
import { RosterController } from './controllers/roster.controller';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Employee } from './models/employee.entity';

@Module({
  imports: [TypeOrmModule.forFeature([Employee])],
  providers: [RosterService],
  controllers: [RosterController],
})
export class RosterModule {}
