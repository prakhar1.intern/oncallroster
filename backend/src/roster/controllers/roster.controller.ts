import {
  Body,
  Controller,
  Delete,
  Get,
  Param,
  Post,
  Put,
} from '@nestjs/common';
import { Observable } from 'rxjs';
import { Employee } from '../models/employee.entity';
import { RosterService } from '../services/roster.service';

@Controller('roster')
export class RosterController {
  constructor(private readonly rosterService: RosterService) {}

  @Post()
  add(@Body() employee: Employee) {
    this.rosterService.insert(employee);
  }

  @Get()
  list(): Observable<Employee[]> {
    return this.rosterService.findAll();
  }

  @Delete(':id')
  remove(@Param() id: number) {
    this.rosterService.remove(id);
  }

  @Delete()
  clear() {
    this.rosterService.removeAll();
  }

  @Put(':id')
  edit(@Body() employee: Employee, @Param('id') id: number) {
    if (id == employee.id) {
      this.rosterService.update(employee);
    }
  }
}
