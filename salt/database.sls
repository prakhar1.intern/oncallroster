ensure database:
  postgres_database.present:
    - name: roster
    - owner: postgres
    - db_user: postgres
    - db_password: vagrant
    - db_host: 192.168.64.201
    - db_port: 5432

# vim: ts=2 sw=2
