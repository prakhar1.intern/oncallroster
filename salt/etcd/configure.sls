install and configure etcd:
  pkg.installed:
    - name: etcd
  file.managed:
    - name: /etc/default/etcd
    - source: salt://etcd/{{ pillar['etcd_config'] }}
  service.running:
    - name: etcd
    - enable: True
    - watch:
      - file: /etc/default/etcd
# delete etcd data-dir:
#  file.directory:
#    - name: /var/lib/etcd
#    - clean: True
#  service.running:
#    - name: etcd
 
# vim: ts=2 sw=2 et
