start etcd service:
  service.running:
    - name: etcd
    - enable: True

# vim: ts=2 sw=2 et
