'curl -fsSL https://deb.nodesource.com/setup_lts.x | sudo -E bash':
  cmd.run:
    - creates: /etc/apt/sources.list.d/nodesource.list
'nodejs':
  pkg.installed
clone app with git:
  git.cloned:
    - name: 'https://gitlab.com/prakhar1.intern/oncallroster.git'
    - target: /srv/roster/
'/srv/roster/backend':
  npm.bootstrap:
    - require:
      - pkg: nodejs
'/srv/roster/frontend':
  npm.bootstrap:
    - require:
      - pkg: nodejs
npm build frontend:
  cmd.run:
    - name: npm run build
    - cwd: /srv/roster/frontend
    - creates: /srv/roster/frontend/out/index.html
npm build backend:
  cmd.run:
    - name: npm run build
    - cwd: /srv/roster/backend
    - creates: /srv/roster/backend/dist/main.js
'/srv/roster/backend/.env':
  file.managed:
    - source: salt://dbenv
'pm2@latest':
  npm.installed
'pm2 start ./dist/main.js':
  cmd.run:
    - cwd: /srv/roster/backend/
    - success_stderr: 'Script already launched, add -f option to force re-execution'
'/etc/systemd/system/roster.service':
  file.managed:
    - source: salt://roster.service
'roster':
  service.running:
    - enable: true
    - watch:
      - file: /etc/systemd/system/roster.service
'pm2 save':
  cmd.run:
    - creates: /etc/.pm2/dump.pm2

# vim: ts=2 sw=2 ft=yaml et
