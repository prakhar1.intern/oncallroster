setting database environ:
  environ.setenv:
  - value: 
      POSTGRES_PORT: 5432
      POSTGRES_USER: postgres
      POSTGRES_PASSWORD: vagrant
      POSTGRES_DATABASE: roster
      POSTGRES_HOST: 192.168.64.202
  cmd.run:
    - name: 'env'

# vim: ts=2 sw=2 ft=yaml et
