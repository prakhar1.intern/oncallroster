# /etc/cloud/templates/hosts.debian.tmpl
'/etc/cloud/templates/hosts.debian.tmpl':
  file.append:
    - source: salt://dbhostnames

db1 in hosts file:
  host.present:
    - name: db3
    - ip: 192.168.64.9
db2 in hosts file:
  host.present:
    - name: db1
    - ip: 192.168.64.7
db3 in hosts file:
  host.present:
    - name: db2
    - ip: 192.168.64.8
salt in hosts file:
  host.present:
    - names:
      - primary
      - salt
    - ip: 192.168.64.2


# vim: ts=2 sw=2 et
