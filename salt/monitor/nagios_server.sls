'apache2':
  pkg.installed
'a2enmod authz_groupfile auth_digest cgi':
  cmd.run

install packages:
  pkg.installed:
    - pkgs:
      - nagios4 
      - nagios-nrpe-plugin 
      - nagios-plugins-contrib


# vim: ts=2 sw=2 ft=yaml et
