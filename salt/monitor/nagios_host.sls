install packages:
  pkg.installed:
    - pkgs:
      - nagios-nrpe-server
      - monitoring-plugins


# vim: ts=2 sw=2 ft=yaml et
