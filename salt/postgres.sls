install postgres:
  pkg.installed:
    - pkgs:
      - postgresql
      - postgresql-contrib
  service.dead:
    - name: postgresql
    - enable: False
  file.absent:
    - name: /var/lib/postgresql/14/main

'ln -s /usr/lib/postgresql/14/bin/* /usr/sbin/':
  cmd.run:
  - creates: /usr/sbin/pg_ctl

# vim: ts=2 sw=2 et
