app1 in hosts file:
  host.present:
    - name: app1
    - ip: 192.168.64.13
app2 in hosts file:
  host.present:
    - name: app2
    - ip: 192.168.64.14
nginx1 in hosts file:
  host.present:
    - name: nginx1
    - ip: 192.168.64.15
nginx2 in hosts file:
  host.present:
    - name: nginx2
    - ip: 192.168.64.16

# vim: ts=2 sw=2 ft=yaml et
