nginx:
  pkg.installed

configure nginx:
  file.managed:
    - name: '/etc/nginx/sites-available/cluster'
    - source: salt://nginx_proxy/{{ pillar['cluster'] }}
  cmd.run:
    - name: 'ln -s /etc/nginx/sites-available/cluster /etc/nginx/sites-enabled/cluster'
    - creates: /etc/nginx/sites-enabled/cluster

'rm /etc/nginx/sites-enabled/default':
  cmd.run:
    - success_stderr: 'No such file or directory'

'nginx -t':
  cmd.run:
  - watch:
    - file: '/etc/nginx/sites-available/cluster'

reload nginx:
  service.running:
    - name: nginx
    - watch:
      - cmd: nginx -t


# vim: ts=2 sw=2 ft=yaml et
