App virtual ip in hosts:
  host.present:
  - name: appserver
  - ip: 192.168.64.200

'keepalived':
  pkg.installed

configure keepalived and restart:
  file.managed:
    - name: /etc/keepalived/keepalived.conf
    - source: salt://nginx_proxy/{{ pillar['keepalived_config'] }}
  service.running:
    - name: keepalived
    - watch:
      - file: /etc/keepalived/keepalived.conf

# vim: ts=2 sw=2 ft=yaml et
