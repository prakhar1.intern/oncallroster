install patroni and required pkgs:
  pkg.installed:
    - pkgs:
      - patroni
      - python-is-python3
      - python3-pip
      - python3-testresources
      - python3-psycopg2
      - python3-etcd

pip3 install setuptools:
  pip.installed:
    - name: 'setuptools'

install config files:
  file.managed:
    - name: /etc/patroni/config.yml
    - source: salt://patroni/{{ pillar['patroni_config'] }}

/data/patroni:
  file.directory:
    - user: postgres
    - group: postgres
    - dir_mode: 700
    - makedirs: True

start service:
  service.running:
  - name: patroni
  - enable: True
  - watch:
    - file: /etc/patroni/config.yml

# vim: ts=2 sw=2 et
