virtualIp1 in hosts:
  host.present:
  - name: virtualIp1
  - ip: 192.168.64.201

virtualIp2 in hosts:
  host.present:
  - name: virtualIp2
  - ip: 192.168.64.202

haproxy1 in hosts:
  host.present:
  - name: haproxy1
  - ip: 192.168.64.10

haproxy2 in hosts:
  host.present:
  - name: haproxy2
  - ip: 192.168.64.11

install haproxy and keepalived:
  pkg.installed:
    - pkgs:
      - haproxy
      - keepalived

configure haproxy and restart:
  file.managed:
    - name: /etc/haproxy/haproxy.cfg
    - source: salt://haproxy/haproxy.cfg
  service.running:
    - name: haproxy
    - watch:
      - file: /etc/haproxy/haproxy.cfg
configure keepalived and restart:
  file.managed:
    - name: /etc/keepalived/keepalived.conf
    - source: salt://haproxy/{{ pillar['keepalived_config'] }}
  service.running:
    - name: keepalived
    - watch:
      - file: /etc/keepalived/keepalived.conf

# vim: ts=2 sw=2 et
