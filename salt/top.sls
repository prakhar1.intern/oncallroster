base:
  '*':
    - hosts
  'db*':
    - postgres
    - etcd.configure
    - patroni.configure
    - database
  'haproxy*':
    - haproxy.configure
  'app*':
    - appserver
  'nginx*':
    - nginx_proxy.nginx
    - nginx_proxy.keepalived
  'monitor':
    - monitor.nagios_server

# vim: ts=2 sw=2 ft=yaml et
