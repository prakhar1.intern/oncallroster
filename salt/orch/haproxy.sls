configure haproxy:
  salt.state:
    - tgt: 'haproxy*'
    - highstage

# vim: ts=2 sw=2 et
